import logging
import threading
import time
import sys

from dyrkdevice.basicDyrkDevice import BasicDyrkDevice

try:
    # checks if you have access to RPi.GPIO, which is available inside RPi
    import RPi.GPIO as GPIO
except ImportError:
    # In case of exception, you are executing your script outside of RPi, so import Mock.GPIO
    import Mock.GPIO as GPIO

try:
    # only works on the pi
    import bme680
except ImportError:
    pass


device = BasicDyrkDevice()
pinout = {0: 40, 1: 38, 2: 36, 3: 32}
GPIO.setmode(GPIO.BOARD)
for pin in pinout.values():
    GPIO.setup(pin, GPIO.OUT)


def time_function(pin, timeout):
    logging.info(f"Turning pin {pin} on for {timeout} seconds")
    GPIO.output(pinout[pin], True)
    logging.info(f"Pin {pin} on")
    time.sleep(timeout)
    logging.info(f"Pin {pin} off")
    GPIO.output(pinout[pin], False)


@device.event(eventName="output_event")
def water(output_state: list):
    threads = []
    for i, value in enumerate(output_state):
        threads.append(threading.Thread(target=time_function, args=(i, value)))

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

@device.event(eventName="data_event")
def data(_):
    pass


if "bme680" in sys.modules:

    @device.measure(measureName="bme680", interval=10)
    def bme():
        try:
            sensor = bme680.BME680(bme680.I2C_ADDR_PRIMARY)
        except (RuntimeError, IOError):
            sensor = bme680.BME680(bme680.I2C_ADDR_SECONDARY)

        return {
            "temperature": sensor.data.temperature,
            "pressure": sensor.data.pressure,
            "humidity": sensor.data.humidity,
        }




device.run()
